package code.challenge.pokedex.common

import android.content.Context
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import code.challenge.pokedex.R


class PokedexTextView @JvmOverloads constructor(context: Context, attrs: AttributeSet? = null, deffStyleAtt: Int = 0): AppCompatTextView(context, attrs, deffStyleAtt) {

        init {
            setTextColor(ContextCompat.getColor(context, R.color.colorText))
            textSize = 16f
            typeface = ResourcesCompat.getFont(context, R.font.broadway_regular)
        }


}
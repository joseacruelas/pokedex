package code.challenge.pokedex.common

import android.widget.ImageView
import androidx.swiperefreshlayout.widget.CircularProgressDrawable
import code.challenge.pokedex.App
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.bitmap.TransformationUtils.centerCrop
import com.bumptech.glide.request.RequestOptions


fun ImageView.loadImage(view: ImageView, url: String) {

    val circularProgressDrawable = CircularProgressDrawable(App.instance)
    circularProgressDrawable.strokeWidth = 5f
    circularProgressDrawable.centerRadius = 30f
    circularProgressDrawable.start()


    val options = RequestOptions().apply {
        centerCrop()
        placeholder(circularProgressDrawable)
    }


    Glide.with(App.instance)
        .load(url)
        .apply(options)
        .into(view)
}
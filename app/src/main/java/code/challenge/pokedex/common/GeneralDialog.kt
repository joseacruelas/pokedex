package com.itexico.android_native.common

import android.content.Context
import android.content.DialogInterface
import androidx.appcompat.app.AlertDialog

class GeneralDialog {

    companion object {
        fun showDialog(
            context: Context,
            title: String,
            message: String,
            positiveButton: String,
            negativeButton: String?,
            neutralButton: String?,
            onClickListener: DialogInterface.OnClickListener?
        ): AlertDialog {
            val builder = AlertDialog.Builder(context)
            builder.setTitle(title)
            builder.setMessage(message)

            builder.setPositiveButton(positiveButton, onClickListener)

            if (negativeButton != null) {
                builder.setNegativeButton(negativeButton, onClickListener)
            }

            if (neutralButton != null) {
                builder.setNegativeButton(neutralButton, onClickListener)
            }

            builder.setCancelable(false)
            return builder.show()
        }
    }
}
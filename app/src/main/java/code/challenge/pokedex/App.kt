package code.challenge.pokedex

import android.app.Application


/**
*
* This class serves to initialize helper classes that we are going to use through the whole application or as general context
*
 **/

class App: Application() {

    companion object {
        lateinit var instance: App
    }

    override fun onCreate() {
        super.onCreate()
        instance = this
    }
}
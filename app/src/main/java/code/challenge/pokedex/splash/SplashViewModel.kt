package code.challenge.pokedex.splash

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import code.challenge.pokedex.App
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.db.PokedexDB
import code.challenge.pokedex.db.dao.SimplePokemonDao
import code.challenge.pokedex.repository.SplashRepository
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.response.PokedexResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class SplashViewModel: ViewModel() {

    private val repository = SplashRepository()
    private val pokemonDao: SimplePokemonDao = PokedexDB.getInstance(App.instance).simplePokemonDAO()
    val data = MutableLiveData<Resource<PokedexResponse>>()

    fun savePokemon(pokemon: List<SimplePokemon>){
        viewModelScope.launch {
            pokemonDao.insert(pokemon)
        }
    }

    fun getPokemon(){
        data.value = Resource.loading(null)
        viewModelScope.launch {
            val response = repository.getPokemon()
            withContext(Dispatchers.Main) {
                data.value = response
            }
        }
    }

}

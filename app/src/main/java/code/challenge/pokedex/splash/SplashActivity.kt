package code.challenge.pokedex.splash

import android.os.Bundle
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.api.Status
import code.challenge.pokedex.base.BaseActivity
import code.challenge.pokedex.databinding.ActivitySplashBinding
import code.challenge.pokedex.main.MainActivity
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.response.PokedexResponse

class SplashActivity: BaseActivity<ActivitySplashBinding>() {

    private val viewModel by lazy {
        ViewModelProvider(this).get(SplashViewModel::class.java)
    }

    private val dataObserver = Observer<Resource<PokedexResponse>>{
        when(it.status){
            Status.SUCCESS -> {
                savePokedex(it.data!!.pokemon)
            }
            Status.ERROR -> {
                showDialog(it.error.toString())
            }
            Status.LOADING -> ""
        }
    }

    override fun getViewBinding() = ActivitySplashBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setObservers()
        init()
    }

    private fun setObservers() {
        viewModel.data.observe(this, dataObserver)
    }

    private fun init(){
        viewModel.getPokemon()
    }
    
    private fun savePokedex(pokemon: List<SimplePokemon>) {
        viewModel.savePokemon(pokemon)
        changeActivity(MainActivity::class.java)
        finish()
    }
}
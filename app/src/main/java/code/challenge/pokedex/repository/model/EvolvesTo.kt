package code.challenge.pokedex.repository.model

import com.google.gson.annotations.SerializedName

data class EvolvesTo (
    @SerializedName("species")
        var species: Species)
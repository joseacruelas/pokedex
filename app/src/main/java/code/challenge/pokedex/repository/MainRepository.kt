package code.challenge.pokedex.repository

import android.os.Handler
import code.challenge.pokedex.App
import code.challenge.pokedex.R
import code.challenge.pokedex.api.ErrorModel
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.base.BaseRepository
import code.challenge.pokedex.db.PokedexDB
import code.challenge.pokedex.db.dao.SimplePokemonDao
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.response.AbilitiesResponse
import code.challenge.pokedex.repository.response.EvoResponse
import code.challenge.pokedex.repository.response.PokemonSpeciesResponse

internal class MainRepository: BaseRepository() {

    private val pokemonDao: SimplePokemonDao = PokedexDB.getInstance(App.instance).simplePokemonDAO()

    suspend fun getPokemonList(): List<SimplePokemon> {
        return pokemonDao.getAllPokemon()
    }

    suspend fun getPokemonSpecies(name: String): Resource<PokemonSpeciesResponse> {
        val data = apiService.getPokemonSpecies(String.format(context.getString(R.string.url_species), name))

        return if(data.isSuccessful){
            Resource.success(data.body()!!)
        } else {
            Resource.error(
                ErrorModel(
                type = ErrorModel.Type.TOLERABLE,
                message = data.message()),
                null)
        }
    }

    suspend fun getPokemonEvo(url: String): Resource<EvoResponse>{
        val data = apiService.getEvo(url)

        return if (data.isSuccessful){
            Resource.success(data.body()!!)
        } else {
            Resource.error(
                ErrorModel(
                    type = ErrorModel.Type.TOLERABLE,
                    message = data.message()),
                null)
        }
    }

    suspend fun setFavorite(name: String, isFavorite: Boolean): Boolean{
        pokemonDao.isFavorite(name, isFavorite)
        return   Handler().postDelayed({
            true
        }, 10000)
    }

    suspend fun getAbilities(name: String): Resource<AbilitiesResponse>{
        val data = apiService.getAbilities(String.format(context.getString(R.string.url_abilities), name))

        return if(data.isSuccessful){
            Resource.success(data.body()!!)
        } else {
            Resource.error(
                    ErrorModel(
                            type = ErrorModel.Type.TOLERABLE,
                            message = data.message()),
                    null)
        }
    }

}
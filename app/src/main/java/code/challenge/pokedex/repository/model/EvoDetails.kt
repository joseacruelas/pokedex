package code.challenge.pokedex.repository.model

import com.google.gson.annotations.SerializedName

data class EvoDetails(
    @SerializedName("evolves_to")
    var evoTo: List<EvolvesTo>

)
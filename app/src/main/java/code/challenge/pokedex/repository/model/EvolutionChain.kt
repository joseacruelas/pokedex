package code.challenge.pokedex.repository.model

import com.google.gson.annotations.SerializedName

data class EvolutionChain (
        @SerializedName("url")
        var url: String)
package code.challenge.pokedex.repository.model

data class EggGroup(
        val name: String,
        val url: String)

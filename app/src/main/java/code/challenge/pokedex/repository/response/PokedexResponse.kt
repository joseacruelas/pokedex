package code.challenge.pokedex.repository.response

import code.challenge.pokedex.repository.model.SimplePokemon
import com.google.gson.annotations.SerializedName

data class PokedexResponse (

    @SerializedName("results")
    var pokemon: List<SimplePokemon>
)
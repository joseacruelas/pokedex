package code.challenge.pokedex.repository.response

import code.challenge.pokedex.repository.model.Ability
import com.google.gson.annotations.SerializedName

data class AbilitiesResponse (
            @SerializedName("abilities")
            val abilitiesList: List<Ability>
        )
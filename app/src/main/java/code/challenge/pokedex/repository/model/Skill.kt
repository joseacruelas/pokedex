package code.challenge.pokedex.repository.model

import com.google.gson.annotations.SerializedName

data class Skill (
        @SerializedName("name")
        val name: String,
        @SerializedName("url")
        val url: String,)
package code.challenge.pokedex.repository.response

import code.challenge.pokedex.repository.model.EvoDetails
import com.google.gson.annotations.SerializedName

data class EvoResponse(
    @SerializedName("chain")
    var chain: EvoDetails

)

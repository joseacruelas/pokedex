package code.challenge.pokedex.repository.response

import code.challenge.pokedex.repository.model.EggGroup
import code.challenge.pokedex.repository.model.EvolutionChain
import com.google.gson.annotations.SerializedName

data class PokemonSpeciesResponse(
    @SerializedName("base_happiness")
    val baseHappiness: Int,
    @SerializedName("capture_rate")
    val captureRate: Int,
    @SerializedName("egg_groups")
    val eggGroup: List<EggGroup>,
    @SerializedName("evolution_chain")
    val evo: EvolutionChain)
package code.challenge.pokedex.repository.model

import com.google.gson.annotations.SerializedName

data class Ability (
   @SerializedName("ability")
   val skill: Skill
)
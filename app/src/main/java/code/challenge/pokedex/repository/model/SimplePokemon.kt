package code.challenge.pokedex.repository.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable

@Entity
data class SimplePokemon (
        @PrimaryKey
        val name: String,
        val url: String,
        val isFavorite: Boolean): Serializable
package code.challenge.pokedex.repository

import code.challenge.pokedex.R
import code.challenge.pokedex.api.ErrorModel
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.base.BaseRepository
import code.challenge.pokedex.repository.response.PokedexResponse

class SplashRepository: BaseRepository() {

   suspend fun getPokemon(): Resource<PokedexResponse> {

       val data = apiService.getPokemon(context.getString(R.string.url_pokedex))

       return if (data.isSuccessful){
           Resource.success(data.body()!!)
       } else {
           Resource.error(ErrorModel(
                   type = ErrorModel.Type.TOLERABLE,
                   message = data.message()),
                   null)
       }
   }

}
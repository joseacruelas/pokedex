package code.challenge.pokedex.repository.model.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import code.challenge.pokedex.R
import code.challenge.pokedex.common.PokedexTextView
import code.challenge.pokedex.repository.model.Species

class SpeciesAdapter (private val speciesList: List<Species>,
                      private val onClick: OnSpeciesClicked): RecyclerView.Adapter<SpeciesAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val cvPokemon: CardView = view.findViewById(R.id.cvPokemon)
        val tvName: PokedexTextView = view.findViewById(R.id.txt_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = speciesList[position]

        holder.tvName.text = pokemon.name
        holder.cvPokemon.setOnClickListener {
            onClick.onClick(
                    pokemon = speciesList[position],
                    view = holder.cvPokemon)
        }
    }

    override fun getItemCount(): Int = speciesList.size

    interface OnSpeciesClicked{
        fun onClick(pokemon: Species, view: View)
    }
}
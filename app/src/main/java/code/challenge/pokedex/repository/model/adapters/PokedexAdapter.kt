package code.challenge.pokedex.repository.model.adapters

import android.os.Handler
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import code.challenge.pokedex.R
import code.challenge.pokedex.common.PokedexTextView
import code.challenge.pokedex.repository.model.SimplePokemon

class PokedexAdapter (private val pokemonList: List<SimplePokemon>,
                      private val onClick: OnPokemonClicked,
                      private val isUpdated: Boolean): RecyclerView.Adapter<PokedexAdapter.ViewHolder>() {

    class ViewHolder(view: View): RecyclerView.ViewHolder(view){
        val cvPokemon: CardView = view.findViewById(R.id.cvPokemon)
        val tvName: PokedexTextView = view.findViewById(R.id.txt_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.pokemon_item, parent, false)
        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val pokemon = pokemonList[position]

        if(isUpdated){
            holder.tvName.text = if (pokemon.isFavorite) "Favorito - ${pokemon.name}" else "Error - ${pokemon.name}"

            Handler().postDelayed({
                holder.tvName.text = pokemon.name
                notifyDataSetChanged()
            }, 5000)

        } else {
            holder.tvName.text = pokemon.name
        }

        holder.cvPokemon.setOnClickListener {
            onClick.onPokemonClicked(
                    pokemon = pokemonList[position],
                    view = holder.cvPokemon)
        }
    }

    override fun getItemCount(): Int = pokemonList.size

    interface OnPokemonClicked{
        fun onPokemonClicked(pokemon: SimplePokemon, view: View)
    }
}
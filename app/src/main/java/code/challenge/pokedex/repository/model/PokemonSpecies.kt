package code.challenge.pokedex.repository.model

data class PokemonSpecies (
        val baseHappiness: Int,
        val captureRate: Int,
        val eggGroup: List<EggGroup>
)
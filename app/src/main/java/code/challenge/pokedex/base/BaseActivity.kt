package code.challenge.pokedex.base

import android.app.ProgressDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.ViewDataBinding
import code.challenge.pokedex.App
import code.challenge.pokedex.R
import com.itexico.android_native.common.GeneralDialog

/**
 *
 *  BaseActivity<@B = View to bind>
 *  This class works as base to all activities, we can declare general functions to use through all activities
 *
 */

abstract class BaseActivity<B: ViewDataBinding>: AppCompatActivity() {

    lateinit var binding: B

    abstract fun getViewBinding(): B

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = getViewBinding()
        setContentView(binding.root)
    }

    fun changeActivity(activity: Class<*>){
        val intent = Intent(App.instance, activity)
        startActivity(intent)
    }

    fun showDialog(error: String) {
        GeneralDialog.showDialog(this,
            getString(R.string.dialog_error_title),
            error,
            getString(R.string.dialog_positive_button),
            null,
            null,
            DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
            })
    }

    fun showProgressDialog(): ProgressDialog {
        return ProgressDialog(this).apply {
            title = "Please Wait"
        }
    }
}
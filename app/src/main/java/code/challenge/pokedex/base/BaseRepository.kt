package code.challenge.pokedex.base

import code.challenge.pokedex.App
import code.challenge.pokedex.api.ApiManager
import code.challenge.pokedex.api.ApiService


/**
 *
 * Base repository to add functions and variables to use along all the repositories
 *
 */
open class BaseRepository {

    val apiService: ApiService = ApiManager.buildRetrofit()
    val context = App.instance
}
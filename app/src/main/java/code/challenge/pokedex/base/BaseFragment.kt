package code.challenge.pokedex.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.ViewDataBinding
import androidx.fragment.app.Fragment

/**
 *
 *  BaseActivity<@B = View to bind>
 *  This class works as base to all activities, we can declare general functions to use through all activities
 *
 */

abstract class BaseFragment<B: ViewDataBinding>: Fragment() {

    lateinit var binding: B

    abstract fun getViewBinding(): B

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = getViewBinding()
        return binding.root
    }
}
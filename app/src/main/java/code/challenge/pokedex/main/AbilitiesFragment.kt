package code.challenge.pokedex.main

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import code.challenge.pokedex.R
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.api.Status
import code.challenge.pokedex.base.BaseFragment
import code.challenge.pokedex.databinding.FragmentSkillsBinding
import code.challenge.pokedex.repository.response.AbilitiesResponse
import code.challenge.pokedex.repository.model.adapters.AbilitiesAdapter

class AbilitiesFragment: BaseFragment<FragmentSkillsBinding>() {

    private lateinit var progressDialog: ProgressDialog

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val abilitiesObserver = Observer<Resource<AbilitiesResponse>>{
        when(it.status){
            Status.SUCCESS -> {
                progressDialog.cancel()
                setUpData(it.data!!)
            }

            Status.ERROR -> {
                progressDialog.cancel()
                (requireActivity() as MainActivity).showDialog(it.error.toString())
            }

            Status.LOADING -> {
                progressDialog = (requireActivity() as MainActivity).showProgressDialog()
                progressDialog.show()
            }
        }
    }

    override fun getViewBinding()  = FragmentSkillsBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        init()
    }

    private fun init() {
        (requireActivity() as MainActivity).setTitle(getString(R.string.txt_skills))
        val name = arguments?.get("name") as String
        viewModel.getAbilities(name)
    }

    private fun setObservers() {
        viewModel.abilities.observe(viewLifecycleOwner, abilitiesObserver)
    }

    private fun setUpData(data: AbilitiesResponse) {
            val adapterPokemon = AbilitiesAdapter(data.abilitiesList)
            binding.rvAbilities.apply {
                layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
                adapter = adapterPokemon
            }
    }


}
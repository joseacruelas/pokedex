package code.challenge.pokedex.main

import android.os.Bundle
import code.challenge.pokedex.base.BaseActivity
import code.challenge.pokedex.databinding.ActivityMainBinding


/**
 *
 *  Activity works only as a container
 *
 */
class MainActivity : BaseActivity<ActivityMainBinding>() {

    var isUpdated = false

    override fun getViewBinding(): ActivityMainBinding = ActivityMainBinding.inflate(layoutInflater)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    fun setTitle(title: String){
        supportActionBar?.title = title
    }

}
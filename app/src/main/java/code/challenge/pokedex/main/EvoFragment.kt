package code.challenge.pokedex.main

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.ViewModelProvider
import code.challenge.pokedex.api.Status
import code.challenge.pokedex.base.BaseFragment
import code.challenge.pokedex.databinding.FragmentEvoBinding
import code.challenge.pokedex.repository.response.EvoResponse
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import code.challenge.pokedex.R
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.repository.model.Species
import code.challenge.pokedex.repository.model.adapters.SpeciesAdapter
import kotlin.random.Random

class EvoFragment: BaseFragment<FragmentEvoBinding>(), SpeciesAdapter.OnSpeciesClicked {

    private lateinit var progressDialog: ProgressDialog
    private lateinit var evoUrl: String

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val evoObserver = Observer<Resource<EvoResponse>> {
        when(it.status){
            Status.SUCCESS -> {
                progressDialog.cancel()
                setData(it.data!!)
            }

            Status.ERROR -> {
                progressDialog.cancel()
                (requireActivity() as MainActivity).showDialog(it.error.toString())
            }

            Status.LOADING -> {
                progressDialog = (requireActivity() as MainActivity).showProgressDialog()
                progressDialog.show()
            }
        }
    }

    private val isFavoriteObserver = Observer<Boolean> {
        if(it){
            (requireActivity() as MainActivity).changeActivity(MainActivity::class.java)
            (requireActivity() as MainActivity).finish()
        }
    }

    override fun getViewBinding()  = FragmentEvoBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        init()
    }

    override fun onClick(pokemon: Species, view: View) {
        viewModel.setFavorite(pokemon.name, Random.nextBoolean())
        (requireActivity() as MainActivity).isUpdated = true
    }

    private fun init() {
        (requireActivity() as MainActivity).setTitle(getString(R.string.txt_evo))
        evoUrl = arguments?.getString("evo") as String
        viewModel.getPokemonEvo(evoUrl)
    }

    private fun setObservers(){
        viewModel.evo.observe(viewLifecycleOwner, evoObserver)
        viewModel.isFavorite.observe(viewLifecycleOwner, isFavoriteObserver)
    }

   private fun setData(response: EvoResponse){
       val species = listOf(response.chain.evoTo[0].species)
       val adapterPokemon = SpeciesAdapter(species, this)
       binding.rvSpecies.apply {
           layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
           adapter = adapterPokemon
       }
   }


}
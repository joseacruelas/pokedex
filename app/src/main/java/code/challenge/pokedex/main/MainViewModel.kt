package code.challenge.pokedex.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.repository.MainRepository
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.response.AbilitiesResponse
import code.challenge.pokedex.repository.response.EvoResponse
import code.challenge.pokedex.repository.response.PokemonSpeciesResponse
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainViewModel: ViewModel() {

   private val repository = MainRepository()

    var pokemonList: MutableLiveData<List<SimplePokemon>> = MutableLiveData()
    val species = MutableLiveData<Resource<PokemonSpeciesResponse>>()
    val evo = MutableLiveData<Resource<EvoResponse>>()
    val isFavorite = MutableLiveData<Boolean>()
    val abilities = MutableLiveData<Resource<AbilitiesResponse>>()

    suspend fun getPokemonList(){
        val list = repository.getPokemonList()
        pokemonList.postValue(list)
    }

    fun getPokemonSpecies(name: String){
        species.value = Resource.loading(null)
        viewModelScope.launch {
            val response = repository.getPokemonSpecies(name)
            withContext(Dispatchers.Main){
                species.value = response
            }
        }
    }

    fun getPokemonEvo(url: String){
        evo.value = Resource.loading(null)
        viewModelScope.launch {
            val response = repository.getPokemonEvo(url)
            withContext(Dispatchers.Main){
                evo.value = response
            }
        }
    }

    fun setFavorite(name: String, isFav: Boolean){
        viewModelScope.launch {
            val result = repository.setFavorite(name, isFav)
            withContext(Dispatchers.Main){
                isFavorite.value = result
            }
        }
    }

    fun getAbilities(name: String){
        abilities.value = Resource.loading(null)
        viewModelScope.launch {
            val response = repository.getAbilities(name)
            withContext(Dispatchers.Main){
                abilities.value = response
            }
        }
    }


}
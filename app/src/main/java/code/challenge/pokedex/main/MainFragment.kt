package code.challenge.pokedex.main

import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import code.challenge.pokedex.base.BaseFragment
import code.challenge.pokedex.databinding.FragmentMainBinding
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.model.adapters.PokedexAdapter
import kotlinx.coroutines.launch

class MainFragment: BaseFragment<FragmentMainBinding>(), PokedexAdapter.OnPokemonClicked {

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val pokemonListObserver = Observer<List<SimplePokemon>> {
        val adapterPokemon = PokedexAdapter(it, this, (requireActivity() as MainActivity).isUpdated)
        binding.rvPokemon.apply {
            layoutManager = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
            adapter = adapterPokemon
        }
    }

    override fun getViewBinding() = FragmentMainBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        init()
    }

    override fun onPokemonClicked(pokemon: SimplePokemon, view: View) {
        goToSpecies(pokemon = pokemon)
    }

    private fun setObservers() {
        viewModel.pokemonList.observe(viewLifecycleOwner, pokemonListObserver)
    }

    private fun init(){
        lifecycleScope.launch {
            viewModel.getPokemonList()
        }
    }

    private fun goToSpecies(pokemon: SimplePokemon){
        val action = MainFragmentDirections.actionMainFragmentToSpeciesFragment(pokemon)
        findNavController().navigate(action)
    }
}
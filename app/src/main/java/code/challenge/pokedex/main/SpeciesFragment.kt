package code.challenge.pokedex.main

import android.app.ProgressDialog
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import code.challenge.pokedex.R
import code.challenge.pokedex.api.Resource
import code.challenge.pokedex.api.Status
import code.challenge.pokedex.base.BaseFragment
import code.challenge.pokedex.databinding.FragmentSpeciesBinding
import code.challenge.pokedex.repository.model.SimplePokemon
import code.challenge.pokedex.repository.response.PokemonSpeciesResponse

class SpeciesFragment: BaseFragment<FragmentSpeciesBinding>() {

    private lateinit var pokemon: SimplePokemon
    private lateinit var progressDialog: ProgressDialog
    private lateinit var evo: String

    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(this).get(MainViewModel::class.java)
    }

    private val pokemonSpeciesObserver = Observer<Resource<PokemonSpeciesResponse>> {
        when(it.status){
            Status.SUCCESS -> {
                progressDialog.cancel()
                setUpData(it.data!!)
                evo = it.data.evo.url
            }

            Status.ERROR -> {
                progressDialog.cancel()
                (requireActivity() as MainActivity).showDialog(it.error.toString())
            }

            Status.LOADING -> {
                progressDialog = (requireActivity() as MainActivity).showProgressDialog()
                progressDialog.show()
            }
        }
    }

    override fun getViewBinding() = FragmentSpeciesBinding.inflate(layoutInflater)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setObservers()
        init()
    }

    private fun setObservers() {
        viewModel.species.observe(viewLifecycleOwner, pokemonSpeciesObserver)
    }

    private fun init() {
        (requireActivity() as MainActivity).setTitle(getString(R.string.txt_species))
        pokemon = arguments?.get("pokemon") as SimplePokemon
        viewModel.getPokemonSpecies(pokemon.name)

        binding.btnEvo.setOnClickListener {
            val action = SpeciesFragmentDirections.actionSpeciesFragmentToEvoFragment(evo)
            findNavController().navigate(action)
        }

        binding.btnSkills.setOnClickListener{
            val action = SpeciesFragmentDirections.actionSpeciesFragmentToAbilitiesFragment(pokemon.name)
            findNavController().navigate(action)
        }
    }

    private fun setUpData(pokemonSpecies: PokemonSpeciesResponse) {
        binding.tvName.text = pokemon.name
        binding.tvHappiness.text = String.format(getString(R.string.txt_happiness), pokemonSpecies.baseHappiness.toString())
        binding.tvRatio.text = String.format(getString(R.string.txt_ratio), pokemonSpecies.captureRate.toString())
        val eggs = pokemonSpecies.eggGroup.joinToString { it.name }
        binding.tvEggGroups.text = String.format(getString(R.string.txt_egg_group), eggs)
    }

}
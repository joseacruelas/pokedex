package code.challenge.pokedex.db

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import code.challenge.pokedex.BuildConfig
import code.challenge.pokedex.db.dao.SimplePokemonDao
import code.challenge.pokedex.repository.model.SimplePokemon


@Database(
        entities = [SimplePokemon::class],
        version = 1,
        exportSchema = false)

abstract class PokedexDB: RoomDatabase() {

    abstract fun simplePokemonDAO(): SimplePokemonDao

    companion object {

        @Volatile
        private var instance: PokedexDB? = null

        fun getInstance(context: Context): PokedexDB {
            return instance
                    ?: synchronized(this) {
                        instance
                                ?: buildDatabase(
                                        context
                                ).also { instance = it }
                    }
        }

        private fun buildDatabase(context: Context): PokedexDB {
            return Room.databaseBuilder(
                    context, PokedexDB::class.java,
                    BuildConfig.STORAGE_NAME
            ).build()
        }
    }
}
package code.challenge.pokedex.db.dao


import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import code.challenge.pokedex.repository.model.SimplePokemon

@Dao
interface SimplePokemonDao {

    @Insert(onConflict =  OnConflictStrategy.REPLACE)
    suspend fun insert(pokemon: List<SimplePokemon>)

    @Query("SELECT * FROM SimplePokemon")
    suspend fun getAllPokemon(): List<SimplePokemon>

    @Query("SELECT * FROM SimplePokemon WHERE name = :name")
    suspend fun getPokemon(name: String): SimplePokemon

    @Query("UPDATE SimplePokemon SET isFavorite = :isFavorite WHERE name = :name")
    suspend fun isFavorite(name: String, isFavorite: Boolean)


}
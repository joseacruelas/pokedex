package code.challenge.pokedex.api

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}
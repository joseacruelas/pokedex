package code.challenge.pokedex.api

class ErrorModel(val type: Type, val message: String, val detailType: String? = null) {

    constructor(type: Type, message: String) : this(type, message, null)

    enum class Type {
        FATAL,
        TOLERABLE
    }
}
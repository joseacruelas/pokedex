package code.challenge.pokedex.api


import android.content.DialogInterface
import code.challenge.pokedex.App
import code.challenge.pokedex.BuildConfig
import code.challenge.pokedex.R
import com.itexico.android_native.common.GeneralDialog
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Request
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

object ApiManager {

    fun buildRetrofit(): ApiService {
        return Retrofit.Builder()
            .baseUrl(BuildConfig.BASE_URL)
            .client(makeOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build().create(ApiService::class.java)
    }

    private fun makeOkHttpClient(): OkHttpClient{
        val okHttpClient = OkHttpClient.Builder()
            .connectTimeout(120, TimeUnit.SECONDS)
            .readTimeout(120, TimeUnit.SECONDS)
            .writeTimeout(120, TimeUnit.SECONDS)

        okHttpClient.addInterceptor(ErrorInterceptor())

        return  okHttpClient.build()
    }


    class ErrorInterceptor : Interceptor {
        override fun intercept(chain: Interceptor.Chain): Response {

            val request: Request = chain.request()
            val response = chain.proceed(request)
            when (response.code()) {
                400 -> {
                    showDialog(response.message())
                }
                401 -> {
                    //Show UnauthorizedError Message
                    showDialog(response.message())
                }

                403 -> {
                    //Show Forbidden Message
                    showDialog(response.message())
                }

                404 -> {
                    //Show NotFound Message
                    showDialog(response.message())
                }
            }
            return response
        }
    }

    private fun showDialog(error: String) {
        GeneralDialog.showDialog(App.instance,
            App.instance.getString(R.string.dialog_error_title),
            error,
            App.instance.getString(R.string.dialog_positive_button),
            null,
            null,
            DialogInterface.OnClickListener { dialog, _ ->
                dialog.dismiss()
            })
    }
}
package code.challenge.pokedex.api

import code.challenge.pokedex.repository.response.AbilitiesResponse
import code.challenge.pokedex.repository.response.EvoResponse
import code.challenge.pokedex.repository.response.PokedexResponse
import code.challenge.pokedex.repository.response.PokemonSpeciesResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface ApiService {

    @GET
    suspend fun getPokemon(@Url path: String): Response<PokedexResponse>

    @GET
    suspend fun getPokemonSpecies(@Url path: String): Response<PokemonSpeciesResponse>

    @GET
    suspend fun getEvo(@Url path: String): Response<EvoResponse>

    @GET
    suspend fun getAbilities(@Url path: String): Response<AbilitiesResponse>

}
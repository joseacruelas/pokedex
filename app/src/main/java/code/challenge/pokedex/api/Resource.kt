package code.challenge.pokedex.api

data class Resource<out T>(val status: Status, val data: T?, val error: ErrorModel?) {
    companion object {
        fun <T> success(data: T?): Resource<T> {
            return Resource(
                status = Status.SUCCESS,
                data = data,
                error = null
            )
        }

        fun <T> error(error: ErrorModel, data: T?): Resource<T> {
            return Resource(
                status = Status.ERROR,
                data = data,
                error = error
            )
        }

        fun <T> loading(data: T?): Resource<T> {
            return Resource(
                status = Status.LOADING,
                data = data,
                error = null
            )
        }
    }
}
